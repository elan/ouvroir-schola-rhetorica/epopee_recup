#!/bin/bash
#--
### AnneGF@CNRS 2024
### Après recup.sh et recup_img.sh, intégration des images et création d'une page HTML statique complète
#--


OUTPUT_DIR="output/"
IMG_DIR="img_recup/"
STATIC_WEB_SITE="site/"
cp -r $IMG_DIR $STATIC_WEB_SITE"img"
mkdir -p $STATIC_WEB_SITE

echo ""> generate_pdf.sh
mkdir -p $STATIC_WEB_SITE/pdf

ls $OUTPUT_DIR | while read file
do
  if [[ $file =~ ^sommaire ]]
  then
      vol=`echo $file | sed 's#sommaire_volume_##; s#.html##'`
  else
      vol=`echo $file | sed 's#^volume_##; s#_article.*$##'`
      art=`echo $file | sed 's#.*_article##; s#-.*$##'`
  fi
  echo '<!DOCTYPE html PUBLIC "html">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="utf-8">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous"><script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script><script src="https://use.fontawesome.com/8ec538547e.js"></script><script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
      <style>
        p { text-align: justify; }
        :root {
          --bs-secondary: #cccccc;
        }
        .header {
          background-color: var(--bs-secondary);
          color: var(--bs-dark);
        }	
	.dossier_titre > span {
	  font-size: large !important;
	}
	.dossier_titre {
	  font-size: larger;
	}
	.auteurs {
	  font-size: large !important;
	}
	.article_titre {
	  font-size: xx-large;
	  padding-top: 10px;
	}
	.footnote {
	  margin: 0;
	  font-size: small;
	}
	h3 {
	  margin-top: 10px;
	}
	@media print {
	  a {
	    text-decoration: none;
	    color: inherit;
	    pointer-events: none;
	    cursor: default;
	  }
	  .header {
	    display:none;
	  }
	  h2 {
	    text-align: center !important;
	  }
	}
      </style>
      <body class="main-content m-4">
        <div class="content col-8 offset-2">
          <div class="header text-center p-4 pb-1 mb-0">
	    <img class="mx-auto d-block" src="img/logo_projet_epopee.jpg" style="">
	    <h1>Le Recueil ouvert</h1>
	  </div>
          <div class="header text-sm text-start bg-secondary text-light p-2 mt-0 mb-4">
	    <a class="text-light" href="./index.html">Le Recueil ouvert</a>' >$STATIC_WEB_SITE$file
  
  if [[ $file =~ ^volume ]]
  then
  	echo ' > <a class="text-light" href="' >>$STATIC_WEB_SITE$file
  	echo './sommaire_volume_'$vol'.html">'  >>$STATIC_WEB_SITE$file
  	echo 'Volume '$vol'</a>'               >>$STATIC_WEB_SITE$file
  	echo '<span class="download" style="float:right;"><a class="text-light" href="pdf/revue_epopee_recueil_ouvert_vol_'$vol'_article'$art'.pdf">Télécharger en pdf</a></span>'               >>$STATIC_WEB_SITE$file
  	echo '</div>' >>$STATIC_WEB_SITE$file
      cat $OUTPUT_DIR$file | sed -e '/Image à récupérer depuis/ s#.*image/\([^<]*\)</a>.*#<figure><img src="img/\1"/>#' | sed -e '/Légende à indiquer/ s#.*<p style="color:red;">Légende à indiquer : \([^<]*\)</p>#<figcaption>\1</figcaption></figure>#; s#http://ouvroir-litt-arts.univ-grenoble-alpes.fr/revues/projet-epopee/\([^ <]*\)#http://epopee.elan-numerique.fr/volume_'$vol'_article_\1.html#' >>$STATIC_WEB_SITE$file
  else
      echo '<span class="download" style="float:right;"><a class="text-light" href="pdf/revue_epopee_recueil_ouvert_vol_'$vol'.pdf">Télécharger en pdf</a></span>'               >>$STATIC_WEB_SITE$file
  	echo '</div>
          <div>
      ' >>$STATIC_WEB_SITE$file
      vol=`echo $file | sed 's#sommaire_volume_##; s#.html##'`
      cat $OUTPUT_DIR$file | sed -e 's#<a href="\([^"]*\)"#<a href="volume_'$vol'_article_\1.html"#;' >>$STATIC_WEB_SITE$file
  
  fi
  echo "</div></body></html>" >>$STATIC_WEB_SITE$file
  
  if [[ $file =~ ^sommaire ]]
  then
    echo "wkhtmltopdf --enable-local-file-access --enable-internal-links  "$STATIC_WEB_SITE$file" "$STATIC_WEB_SITE"pdf/revue_epopee_recueil_ouvert_vol_"$vol"_sommaire.pdf">>generate_pdf.sh
  else
    echo "wkhtmltopdf --enable-local-file-access --enable-internal-links  "$STATIC_WEB_SITE$file" "$STATIC_WEB_SITE"pdf/revue_epopee_recueil_ouvert_vol_"$vol"_article"$art".pdf">>generate_pdf.sh
  fi
done

# Bash for generating merged pdf
ls=$STATIC_WEB_SITE"sommaire*"
ls $ls | while read file
do
    echo -ne "pdftk ">>generate_pdf.sh
    vol=`echo $file | sed 's#.*sommaire_volume_##; s#.html##'`
    if [[ "2020 2021 2022" == *"$vol"* ]]
    then
	echo -ne $STATIC_WEB_SITE"pdf/volume_"$vol"_pages_de_garde.pdf ">>generate_pdf.sh
    fi
    echo -ne $STATIC_WEB_SITE"pdf/revue_epopee_recueil_ouvert_vol_"$vol"_sommaire.pdf ">>generate_pdf.sh
    grep -o 'volume_[^-]*' $file | sed 's#.*_##' | while read art
    do
	echo -ne $STATIC_WEB_SITE"pdf/revue_epopee_recueil_ouvert_vol_"$vol"_article_"$art".pdf ">>generate_pdf.sh
    done
    echo "cat output "$STATIC_WEB_SITE"pdf/revue_epopee_recueil_ouvert_vol_"$vol".pdf ; ">>generate_pdf.sh
done
