#!/bin/bash
#--
### AnneGF@CNRS 2024
### À partir des sortie mises en forme (output/), récupération des img depuis le site ramure.net
### Usage
###  bash ./recup_img.sh >do_recup_img.sh
### Vérifier le contenu de do_recup_img.sh au cas où puis lancer
###  bash ./do_recup_img.sh
#---
BASE_URL="http://ouvroir.ramure.net/revues/projet-epopee/"
OUTPUT_DIR="output/"
IMG_DIR="img_recup/"
mkdir -p $IMG_DIR

ls $OUTPUT_DIR | while read file
do
    if [[ `grep '<span style="color:red">' $OUTPUT_DIR$file | wc -l` -gt 0 ]]
    then
	echo "mkdir -p $IMG_DIR"
    else
	echo "#No img for $IMG_DIR${file%%.html}"
    fi
    grep '<span style="color:red">' $OUTPUT_DIR$file | sed 's#.*a href="##; s#".*##' | while read url
    do
	f_img=${url##*image/}
	dir_img=${f_img%%/*}
	echo "mkdir -p $IMG_DIR$dir_img"
	echo "wget $url -O $IMG_DIR$f_img"
    done
done
