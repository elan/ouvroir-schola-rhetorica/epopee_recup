#!/bin/bash
#--
### AnneGF@CNRS 2024
### Articles 228 et 425 : traitement particulier car HTML erroné ; voir plus bas dans ce script
### pour observer les erreurs plus facilement, lancer :
###    bash ./recup.sh 2>log; cat log
### pour observer spécifiquement les entités HTML manquantes et mettre à jour dans ce script la variable $xml_entities
###    bash ./recup.sh 2>log; cat log | grep -o "Entity '[^']*' not defined" | sort | uniq -c | sort -rn
### pour observer spécifiquement les éléments non pris en charge par les XSL et les mettre à jour
###    bash ./recup.sh 2>log; cat log | grep TODO | sort | uniq -c | sort -rn
#---
BASE_URL="http://ouvroir.ramure.net/revues/projet-epopee/"
RECUP_DIR="recup_html/"
OUTPUT_DIR="output/"
mkdir -p $RECUP_DIR $OUTPUT_DIR

xml_entities='<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE nomracine [
  <!ENTITY nbsp "&#160;">
  <!ENTITY icirc "î">
  <!ENTITY acirc "â">
  <!ENTITY aacute "á">
  <!ENTITY Aacute "Á">
  <!ENTITY eacute "é">
  <!ENTITY iacute "í">
  <!ENTITY oacute "ó">
  <!ENTITY uacute "ú">
  <!ENTITY agrave "à">
  <!ENTITY Agrave "À">
  <!ENTITY egrave "è">
  <!ENTITY Egrave "È">
  <!ENTITY ugrave "ù">
  <!ENTITY atilde "ã">
  <!ENTITY otilde "õ">
  <!ENTITY ntilde "ñ">
  <!ENTITY rsquo "’">
  <!ENTITY Eacute "É">
  <!ENTITY auml "ä">
  <!ENTITY euml "ë">
  <!ENTITY iuml "ï">
  <!ENTITY ouml "ö">
  <!ENTITY uuml "ü">
  <!ENTITY yuml "ÿ">
  <!ENTITY ccedil "ç">
  <!ENTITY acirc "â">
  <!ENTITY Acirc "Â">
  <!ENTITY ecirc "ê">
  <!ENTITY Ecirc "Ê">
  <!ENTITY icirc "î">
  <!ENTITY ocirc "ô">
  <!ENTITY ucirc "û">
  <!ENTITY oelig "œ">
  <!ENTITY ndash "–">
  <!ENTITY mdash "—">
  <!ENTITY hellip "&#x2026;">
  <!ENTITY laquo "«">
  <!ENTITY raquo "»">
  <!ENTITY ldquo "&#x201C;">
  <!ENTITY rdquo "&#x201D;">
  <!ENTITY lsquo "‘">
  <!ENTITY rsquo "’">
  <!ENTITY deg "°">
  <!ENTITY ordm "º">
  <!ENTITY scaron "š">
  <!ENTITY iquest "¿">
]>
'


volume="2015"
for page in 251 252 256 316 341 369 392 403 431
do
    echo -e "\e[33mVolume "${volume}"\e[0m"
    out=${RECUP_DIR}"Volume_"${volume}".html"
    if [ ! -f ${out} ]
    then
	wget ${BASE_URL}${page} -O $out
    fi
    echo $xml_entities >tmp
    echo '<xml><div>' >>tmp
    cat $out | sed '1,/<div id="contenu_deco">/ d; /#decoration_contenu/,$ d;' | sed 's#</ul> *</div> *</div> *</div>#</ul></div>#' >> tmp
    echo '</xml>' >>tmp
    mv tmp $out".clean.xml"
    outfinal=${OUTPUT_DIR}sommaire_volume_${volume}.html
    xsltproc -o ${outfinal} html-ouvroir-volume2html-hypotheses.xsl $out".clean.xml"
    echo -e "\e[33m  Sommaire créé : "${outfinal}"\e[0m"
    ## Process articles
    grep -o "a href=[^>]*" $out".clean.xml" | sed 's#a href="##; s#"##' | while read href;
    do
	#echo "  "$href
	out=${RECUP_DIR}"Volume_"${volume}"_Article"$href".html"
	if [ ! -f ${out} ]
	then
	    wget ${BASE_URL}$href -O $out
	fi
	echo $xml_entities >tmp
	echo '<xml><div>' >>tmp
	shopt -s extglob;
	num=`expr "$href" : '^\([0-9]\+\)-.*$'`
	case $num in
	    228)
		cat $out | sed '1,/<div id="enteteDocument">/ d; /#decoration_contenu/,$ d;' | sed 's#<a id="bibliographie-target">##; s#</ul> *</div> *</div> *</div>#</ul></div>#' | sed '$ d' | sed 's#</em></span></span>#</span></em></span>#g' >> tmp;;
	    425)
		cat $out | sed '1,/<div id="enteteDocument">/ d; /#decoration_contenu/,$ d;' | sed 's#<a id="bibliographie-target">##; s#</ul> *</div> *</div> *</div>#</ul></div>#' | sed '$ d' | sed '/p class="resume"/ s#<br /></em></span>#<br /></span></em>#g' >> tmp;;
	    *)
		cat $out | sed '1,/<div id="enteteDocument">/ d; /#decoration_contenu/,$ d;' | sed 's#<a id="bibliographie-target">##; s#</ul> *</div> *</div> *</div>#</ul></div>#' | sed '$ d'>> tmp;;
	esac
	shopt -u extglob;
#	if [[ "$href" =~ ^228.* ]]
#	then
#	    cat $out | sed '1,/<div id="enteteDocument">/ d; /#decoration_contenu/,$ d;' | sed 's#<a id="bibliographie-target">##; s#</ul> *</div> *</div> *</div>#</ul></div>#' | sed '$ d' | sed 's#</em></span></span>#</span></em></span>#g' >> tmp
#	else
##	    
#	fi
	echo '</xml>' >>tmp
	mv tmp $out".clean.xml"
	outfinal=${OUTPUT_DIR}volume_${volume}_article_${href}.html
	xsltproc -o ${outfinal} html-ouvroir2html-hypotheses.xsl ${out}".clean.xml"
    echo -e "\e[33m  Article créé : "${outfinal}"\e[0m"
    done
    volume=$((volume+1))
done
