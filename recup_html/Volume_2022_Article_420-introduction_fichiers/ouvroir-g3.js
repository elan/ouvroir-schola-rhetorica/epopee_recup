var _____WB$wombat$assign$function_____ = function(name) {return (self._wb_wombat && self._wb_wombat.local_init && self._wb_wombat.local_init(name)) || self[name]; };
if (!self.__WB_pmw) { self.__WB_pmw = function(obj) { this.__WB_source = obj; return this; } }
{
  let window = _____WB$wombat$assign$function_____("window");
  let self = _____WB$wombat$assign$function_____("self");
  let document = _____WB$wombat$assign$function_____("document");
  let location = _____WB$wombat$assign$function_____("location");
  //let top = _____WB$wombat$assign$function_____("top");
  let parent = _____WB$wombat$assign$function_____("parent");
  let frames = _____WB$wombat$assign$function_____("frames");
  let opener = _____WB$wombat$assign$function_____("opener");

(function () {

  var OUV;
  if (window.OUV) {
    OUV = window.OUV
  }
  else {
    window.OUV = OUV = {};
  }


  /*
   * Modifier taille des caractères
   * 
   * JS récupéré à partir du site officiel, souvent des bout de JS
   * inclus directement dans la page.
   * 
   */

  var taille = 1;			

  var changerTaille = OUV.changerTaille = function changerTaille(modif) {
    var doc;
    taille = taille + modif;
    if (document.getElementById("contenu_avec_nav_avec_encadres")) {
      //alert("cas 1 : " + document.getElementById("contenu_avec_nav_avec_encadres").style.fontSize);
      doc = "contenu_avec_nav_avec_encadres";	
      document.getElementById("encadres").style.fontSize = taille + "em";
    }
    else if (document.getElementById("contenu_avec_nav_sans_encadres")) {
      doc = "contenu_avec_nav_sans_encadres";		
    }
    else if (document.getElementById("contenu_sans_nav_avec_encadres")) {
      doc = "contenu_sans_nav_avec_encadres";		
      document.getElementById("encadres").style.fontSize = taille + "em";
    }
    else if (document.getElementById("contenu_sans_nav_sans_encadres")) {
      doc = "contenu_sans_nav_sans_encadres";		
    }
    //alert('changerTaille doc = ' +doc);
    document.getElementById(doc).style.fontSize = taille + "em";
  }

  /*
   * Montrer/cacher sous-menu des composantes 
   */

$(document).ready(function () {

  if ($("#menu_secondaire").length > 0) {
    var $toggle_link =  $("#menu_secondaire a.toggle");
    if ($toggle_link.length > 0) {
      $toggle_link.click(function (ev) {
        ev.preventDefault();
        var $sublist = $("ul", $(ev.target).parent());
        $sublist.toggle(700);
      });
    }
  }

  /*
   * Icone imprimer
   */
  if ($("#imprime-moi").length > 0) {
    var $print_link = $("#imprime-moi");
    $print_link.click(function (ev) {
      ev.preventDefault();
      window.print();
    });
  }
});

})();

}
/*
     FILE ARCHIVED ON 16:25:08 Mar 11, 2023 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 08:21:25 Oct 22, 2024.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  captures_list: 0.609
  exclusion.robots: 0.021
  exclusion.robots.policy: 0.01
  esindex: 0.01
  cdx.remote: 9.123
  LoadShardBlock: 196.44 (3)
  PetaboxLoader3.datanode: 85.841 (4)
  PetaboxLoader3.resolve: 149.049 (3)
  load_resource: 42.21
*/