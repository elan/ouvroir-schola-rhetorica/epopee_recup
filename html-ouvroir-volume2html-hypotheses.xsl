<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="1.0">
    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/">
        <div>
            <h2>
                <xsl:apply-templates select="xml/div/h2[@class = 'titre']"/>
            </h2>
            <p>
                <xsl:apply-templates select="xml/div/h3[@class = 'direction']"/>
            </p>
            <xsl:apply-templates select="xml/div/div[@class = 'description']"/>
            <p>
                <ul>
                    <xsl:for-each select="xml/div/ul/li">
                        <li><xsl:apply-templates/></li>
                    </xsl:for-each>
                </ul>
            </p>
        </div>
    </xsl:template>

    <xsl:template
        match="div | h2 | h3 | span[@class = 'sousledir'] | span[@class = 'familyName']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="p | strong | em | sup">
        <xsl:element name="{name()}">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="span[@lang]|span[@style='text-transform:uppercase;']">
        <xsl:copy-of select="."/>
    </xsl:template>
    
    <xsl:template match="br">
        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
    </xsl:template>

    <xsl:template match="a">
        <a href="{@href}"><xsl:apply-templates/></a>
    </xsl:template>

    <xsl:template match="*">
        <xsl:message>TODO : <xsl:value-of select="name()"/> [<xsl:copy-of select="."/>]</xsl:message>
    </xsl:template>
</xsl:stylesheet>
