Extraction des pages lodel de l'ouvroir/épopée pour :
  * création de pages de blog hypothèses : voir dossier output/ (des copier/coller donnent un résultat pas mal, mais il faut encore s'occuper des images)
  * création d'un site statique : voir dossier site/ et explication ci-après pour ne cloner que cette partie là du dépôt

## Clone only the static site
git clone -n --depth=1 --filter=tree:0 https://gricad-gitlab.univ-grenoble-alpes.fr/elan/ouvroir-schola-rhetorica/epopee_recup.git $DIR
cd $DIR
git sparse-checkout set --no-cone site
git checkout

## Generate pdf
sudo aptitude install wkhtmltopdf
wkhtmltopdf --enable-local-file-access $HTML_FILE $PD_FFILE


## TODO
  * bloc citation : que faire ?
  * titre, etc à voir