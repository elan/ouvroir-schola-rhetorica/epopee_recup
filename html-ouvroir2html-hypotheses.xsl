<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="1.0">

    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <xsl:apply-templates select="xml/div/h2[@class = 'publiParente']"/>
        <xsl:apply-templates select="xml/div[@id = 'resume']"/>
        <xsl:apply-templates select="xml/h4[@class = 'texteIntegral']"/>
        <xsl:apply-templates select="xml/div[@id = 'texte']"/>
        <hr/>
        <xsl:for-each select="//div[@class = 'notes']//p[@class = 'notesbaspage']">
            <xsl:apply-templates select="." mode="do"/>
        </xsl:for-each>
        <xsl:apply-templates select="//div[@id = 'pourCiter']"/>
        <xsl:apply-templates select="//h4[@id = 'surLauteur']"/>
        <!--<xsl:apply-templates select="//div[@class = 'descriptionauteur']/p[@class = 'description']"-->
    </xsl:template>

    <xsl:template match="h2[@class = 'publiParente']">
        <h2 class="dossier_titre" style="position:relative; z-index:1;text-align: end;">
            <img style="position: absolute; z-index: -1; righ: 0; height: 100px; box-shadow:none;"
                src="http://ouvroir.ramure.net/revues/actalittarts/css/img/esper-grise-150.png"/>
            <span style="color:#175676; font-size:xx-large;">
                <i>
                    <xsl:apply-templates select="a[@class = 'dossierTitre']"/>
                </i>
            </span>
        </h2>
        <xsl:apply-templates select="following-sibling::h2[@class = 'pane-title titre-article']"/>
        <xsl:apply-templates select="following-sibling::div[@class = 'auteurs']"/>
        <xsl:apply-templates select="following-sibling::div[@id = 'resume']"/>

    </xsl:template>

    <xsl:template match="//div[@class = 'notes']//p[@class = 'notesbaspage']" mode="do">
        <p class="footnote">
            <a class="footnotesymbol" id="{a/@id}" href="{a/@href}">
                <xsl:value-of select="a"/>
            </a>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="div[@id = 'resume']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="div[@id = 'texte']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="h2[@class = 'pane-title titre-article']">
        <h2 class="article_titre" style="z-index: 1; position: relative; text-align: end;">
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <xsl:template match="h2[@class = 'texte']">
        <h2>
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <xsl:template match="h3[@class = 'texte']">
        <h3>
            <xsl:apply-templates/>
        </h3>
    </xsl:template>

    <xsl:template match="h4[@class = 'texteIntegral']">
        <h4>
            <xsl:apply-templates/>
        </h4>
    </xsl:template>

    <xsl:template match="h4">
        <h4>
            <xsl:if test="parent::div[@id = 'resume']">
                <xsl:attribute name="style">
                    <xsl:text>margin-bottom: 6px;</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </h4>
    </xsl:template>

    <xsl:template match="p[@class = 'resume']">
        <p class="resume" style="text-align: justify; line-height: 120% !important;">
            <span style="color: #808080;">
                <xsl:apply-templates/>
            </span>
        </p>
    </xsl:template>

    <xsl:template match="blockquote">
        <blockquote>
            <xsl:apply-templates/>
        </blockquote>
    </xsl:template>

    <xsl:template match="a[@class = 'dossierTitre']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="div[@class = 'auteurs']">
        <h2 class="auteurs"
            style="z-index: 1; position: relative; text-align: end; font-size: 11pt;">
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <xsl:template match="p[@class = 'encadre']">
        <p class="encadre"
            style="background-color: #efefef; margin-bottom: 0; padding: 0 5px 1em 5px;">
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="p[@class = 'epigraphe']">
        <p class="epigraphe">
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="
            p[@class = 'standarduser'] |
            p[@class = 'paragraphesansretrait'] |
            p[@class = 'corps'] |
            p[@class = 'commentaireinterne']">
        <!-- à vérifier auprès des équipes... -->
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="p[@class = 'citation' and parent::blockquote]">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="p[@class = 'citation' and not(parent::blockquote)]">
        <blockquote>
            <xsl:apply-templates/>
        </blockquote>
    </xsl:template>

    <xsl:template match="p[@class = 'bibliographie']">
        <p class="bibliographie" style="font-size:small">
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="
            p[@class = 'texte'] | p[@class = 'pardefaut'] |
            p[@class = 'aparagraphepolice14']">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="h1[@class = 'texte']">
        <h3>
            <xsl:apply-templates/>
        </h3>
    </xsl:template>

    <xsl:template match="span[@class = 'paranumber']"/>

    <xsl:template match="span[@class = 'lettrine']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="
            span[@style = 'font-variant:small-caps;'] |
            span[@style = 'font-variant:small-caps'] |
            span[@class = 'smallcaps']">
        <span style="font-variant:small-caps;">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="
            span[@lang] |
            span[@xml:lang] |
            span[@style = 'text-transform:uppercase;'] |
            span[@style = 'font-weight:normal;']">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="
            span[@class = 'familyName'] | span[@class = 'nomfamille'] | span[@class = 'texte']
            | span[@class = 'image_error']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="
            span[@style = 'text-decoration:underline;'] |
            span[@style = 'text-decoration:underline']">
        <u>
            <xsl:apply-templates/>
        </u>
    </xsl:template>

    <xsl:template match="sup[a[@class = 'footnotecall']]">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sup">
        <sup>
            <xsl:apply-templates/>
        </sup>
    </xsl:template>

    <xsl:template match="del">
        <s>
            <xsl:apply-templates/>
        </s>
    </xsl:template>

    <xsl:template match="em">
        <em>
            <xsl:apply-templates/>
        </em>
    </xsl:template>

    <xsl:template match="i">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>

    <xsl:template match="strong">
        <strong>
            <xsl:apply-templates/>
        </strong>
    </xsl:template>

    <xsl:template match="ul[@class = 'notes-marge']"/>

    <xsl:template match="ul[@class = 'texte']">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>

    <xsl:template match="li[parent::ul[@class = 'texte']]">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>

    <xsl:template match="table[@class = 'texte']">
        <table id="{@id}" dir="{@dir}">
            <xsl:apply-templates/>
        </table>
    </xsl:template>

    <xsl:template match="tr[parent::table[@class = 'texte']]">
        <tr>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </tr>
    </xsl:template>

    <xsl:template match="td">
        <td>
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates/>
        </td>
    </xsl:template>

    <xsl:template match="a[@class = 'footnotecall']">
        <sup>
            <a href="{@href}" id="{@id}">
                <xsl:value-of select="."/>
            </a>
        </sup>
        <!--<span title="note"><xsl:text> ((</xsl:text>
        <xsl:apply-templates select="//a[substring-after(@href, '#') = current()/@id]/parent::p"/>
        <xsl:text>))</xsl:text></span>-->
    </xsl:template>

    <xsl:template match="a[@class = 'FootnoteSymbol']"/>

    <xsl:template match="a[@class = 'auteur']">
        <xsl:apply-templates/>
    </xsl:template>

    <!--<xsl:template match="h1[@class = 'texte']/a">
        <xsl:apply-templates/>
    </xsl:template>-->

    <xsl:template match="a[@class = 'footnotecall'][starts-with(name(parent::*), 'h')]" priority="1">
        <sup>
            <a href="{@href}" id="{@id}">
                <xsl:value-of select="."/>
            </a>
        </sup>
    </xsl:template>
    <xsl:template match="a[starts-with(name(parent::*), 'h')]">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="a[@href]" priority="0">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="div[@id = 'pourCiter']">
        <h3>Pour citer ce document</h3>
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="h4[@id = 'surLauteur']">
        <h3>
            <xsl:apply-templates/>
        </h3>
        <xsl:apply-templates
            select="following-sibling::div[@class = 'descriptionauteur'][1]/p[@class = 'description']"
            mode="do"/>
    </xsl:template>

    <xsl:template match="div[@class = 'descriptionauteur']/p[@class = 'description']" mode="do">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="div[@class = 'descriptionauteur']/p[@class = 'description']"/>

    <xsl:template match="p[@class = 'notesbaspage'] | p[@class = 'normalweb']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="img">
        <span style="color:red">Image à récupérer depuis </span>
        <a href="http://ouvroir.ramure.net/revues/projet-epopee/{@src}">
            <xsl:text>http://ouvroir.ramure.net/revues/projet-epopee/</xsl:text>
            <xsl:value-of select="@src"/>
        </a>
    </xsl:template>

    <xsl:template match="p[@class = 'puces' and parent::li]">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="p[@class = 'legendeillustration']">
        <p style="color:red;">Légende à indiquer : <xsl:value-of select="."/>
        </p>
    </xsl:template>

    <xsl:template match="p[@class = 'creditillustration']">
        <p style="color:red;">Crédit illustration à indiquer : <xsl:value-of select="."/>
        </p>
    </xsl:template>
    <xsl:template match="p[@class = 'titreillustration']">
        <p style="color:red;">Titre illustration à indiquer : <xsl:value-of select="."/>
        </p>
    </xsl:template>

    <xsl:template match="span[@class = 'citation,quotation,quotations,citatextual,citastextuales']">
        <blockquote>
            <xsl:apply-templates/>
        </blockquote>
    </xsl:template>

    <xsl:template match="br[position() = 1]"/>

    <xsl:template match="br">
        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
    </xsl:template>

    <xsl:template match="*">
        <xsl:message>
            <xsl:text>TODO : </xsl:text>
            <xsl:value-of select="name()"/>
            <xsl:text> [</xsl:text>
            <xsl:for-each select="@*">
                <xsl:text>@</xsl:text>
                <xsl:value-of select="name()"/>
                <xsl:text>="</xsl:text>
                <xsl:value-of select="."/>
                <xsl:text>" </xsl:text>
            </xsl:for-each>
            <xsl:copy-of select="."/>
            <xsl:text>]</xsl:text>
        </xsl:message>
    </xsl:template>
</xsl:stylesheet>
